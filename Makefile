all: client server

client: client.o
	gcc -g -o client client.c

server: server.o
	gcc -g -o server server.c

clean:
	rm -rf *.o *.dSYM client server