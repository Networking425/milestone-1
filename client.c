/**
 *  client.c
 *  @author: Hercy (Yunhao Zhang)
 *  @author: Xingyi Yan
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>


#define DEBUG_FLAG  0       // Set to 1 for debuging
#define MAX_LENGTH 	1024
#define LENGTH_FID 	4

#define SUCCESS		0
#define USER_ERROR -2
#define SYSM_ERROR -1
/*
 struct in_addr
 {
	in_addr_t s_addr; // 32-bit IP address
 };
 
 struct sockaddr_in
 {
	short int sin_family;			// Address family
	unsigned short int sin_port;	// Port number, 16 bit
	struct in_addr sin_addr;		// Internet address, 32 bit
	unsigned char sin_zero[8];		// Use memset to set it zero
 };
 */
void chopNewLine(char *str);
void printUsage();

int i; // Super useful looping index

int main(int argc, char *argv[])
{
    
    if(DEBUG_FLAG)
    {
        printf("*** @main: Client strated!\n");
    }
    
    
    /* Check arguments */
    if(argc != 3)
    {
        printUsage();
        exit(USER_ERROR);
    }
    
    
    /* Initialization */
    if(DEBUG_FLAG)
    {
        printf("*** @main: Initializing... ");
    }
    int s;
    int port = atoi(argv[2]);
    struct sockaddr_in serverAddr;
    char payload[MAX_LENGTH];
    // package : 32bit (4 byte) length & 1024 byte payload
    char * package = (char *) malloc((MAX_LENGTH + LENGTH_FID) * sizeof(char));
    
    if(DEBUG_FLAG)
    {
        printf("OK\n");
    }
    
    /* Process server address */
    if(DEBUG_FLAG)
    {
        printf("*** @main: Processing server address... ");
    }
    memset(&serverAddr, '0', sizeof(serverAddr));
    
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(port);
    int clone = inet_pton(AF_INET, argv[1], &serverAddr.sin_addr);
    if(clone <= 0)
    {
        fprintf(stderr, "Error: Please type in a correct server address!\n");
        exit(USER_ERROR);
    }
    if(DEBUG_FLAG)
    {
        printf("OK\n");
        printf("-----------------------------\n");
    }
    
    memset(payload, 0, sizeof(payload));
    while(fgets(payload, MAX_LENGTH, stdin))
    {
        chopNewLine(payload);

        
        if(DEBUG_FLAG)
        {
            printf("*** @main: Packing packge... ");
        }
        /* Packing package */
        long length = 0;
        while(payload[length])
        {
            length++;
        }
        
        memcpy(package, &length, sizeof(char) * LENGTH_FID);
        
        memcpy(&package[LENGTH_FID], &payload, sizeof(payload));
        
        long num;
        memcpy(&num, package, sizeof(char) * LENGTH_FID);
        
        if(DEBUG_FLAG)
        {
            printf("OK\n");
            
            printf("Length of payload: %ld\n", num);
            
            printf("> %s\n", payload);
        /*
            int i;
            for(i = 0; i < num; i++)
            {
                printf("%d: %c\n", LENGTH_FID + i, package[LENGTH_FID + i]);
            }*/
        }
        
        
        /* Creating new socket */
        if(DEBUG_FLAG)
        {
            printf("*** @main: Creating a socket... ");
        }
        s = socket(AF_INET, SOCK_STREAM, 0);
        if(s < 0)
        {
            fprintf(stderr, "Fail to create a new socket!\n");
            exit(SYSM_ERROR);
        }
        if(DEBUG_FLAG)
        {
            printf("OK\n");
        }
        

        
        /* Connecting to server */
        if(DEBUG_FLAG)
        {
            printf("*** @main: Connecting to server... ");
        }
        int connection = connect(s, (struct sockaddr *) &serverAddr, sizeof(serverAddr));
        if(connection < 0 )
        {
            fprintf(stderr, "Fail to connect!\n");
            exit(SYSM_ERROR);
        }
        if(DEBUG_FLAG)
        {
            printf("OK\n");
        }
        
        if(DEBUG_FLAG)
        {
            printf("*** @main: Delivering package... ");
        }
        send(s, package, LENGTH_FID+MAX_LENGTH, 0);
        if(DEBUG_FLAG)
        {
            printf("OK\n");
        }
        memset(package, '0', sizeof(char)*(LENGTH_FID+MAX_LENGTH));
        
        close(s);
        if(DEBUG_FLAG)
        {
            printf("*** @main: connection closed.\n");
            printf("-----------------------------\n");
        }
    }
    
    
    return SUCCESS;
}

void chopNewLine(char *str)
{
    i = 0;
    while(str[i])
    {
        i++;
    }
    if(str[i - 1] == '\n')
    {
        str[i - 1] = '\0';
    }
}

void printUsage()
{
    printf("Usage:\n");
    printf("client <sip> <sport>\n");
    printf("\tsip   - Server's IP address\n");
    printf("\tsport - Server's port number\n");
}