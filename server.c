/**
 *  server.c
 *  @author: Hercy (Yunhao Zhang)
 *  @author: Xingyi Yan
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <time.h>


#define DEBUG_FLAG  0       // Set to 1 for debuging
#define MAX_LENGTH  1024
#define LENGTH_FID 	4

#define SUCCESS		0
#define USER_ERROR -2
#define SYSM_ERROR -1

void printUsage();

int main(int argc, char *argv[])
{
    if(DEBUG_FLAG)
    {
    	printf("*** @main: Server strating...\n");
    }


    /* Check arguments */
	if(argc != 2)
	{
		printUsage();
		exit(USER_ERROR);
	}


	/* Initialization */
	if(DEBUG_FLAG)
	{
		printf("*** @main: Initializing... ");
	}
	int s, n;
	int port = atoi(argv[1]);
	struct sockaddr_in serverAddr;
    char payload[1024];
    // package : 32bit (4 byte) length & 1024 byte payload
	char * package = (char *) malloc((MAX_LENGTH + LENGTH_FID) * sizeof(char));
	if(DEBUG_FLAG)
	{
		printf("OK\n");
	}


	/* Creating new socket */
	if(DEBUG_FLAG)
	{
		printf("*** @main: Creating a socket... ");
	}	
	s = socket(AF_INET, SOCK_STREAM, 0);
	if(s < 0)
	{
		fprintf(stderr, "Fail to create a new socket!\n");
		exit(SYSM_ERROR);
	}
	if(DEBUG_FLAG)
	{
		printf("OK\n");
	}


	/* Process server address */
	if(DEBUG_FLAG)
	{
		printf("*** @main: Processing server address... ");
	}
	memset(&serverAddr, '0', sizeof(serverAddr));

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddr.sin_port = htons(port);
	if(DEBUG_FLAG)
	{
		printf("OK\n");
        printf("-----------------------------\n");
	}

	bind(s, (struct sockaddr *) &serverAddr, sizeof(serverAddr));

	listen(s, 5);

	while(1)
	{
        memset(package, 0, sizeof(char)*(LENGTH_FID+MAX_LENGTH));
		n = accept(s, (struct sockaddr*) NULL, NULL);
        
        if(DEBUG_FLAG)
        {
            printf("*** @main: Recing package... ");
        }
        recv(n, package, LENGTH_FID+MAX_LENGTH, 0);
        if(DEBUG_FLAG)
        {
            printf("OK\n");
        }
        
        /* Unpack package */
        if(DEBUG_FLAG)
        {
            printf("*** @main: Unpacking package... ");
        }
        long length = 0;
        memcpy(&length, package, sizeof(char) * LENGTH_FID);
        if(DEBUG_FLAG)
        {
            printf("OK\n");
        }
        printf("Length of payload: %ld\n", length);
        
        //memcpy(&payload, &package[length + 1], sizeof(char) * length);
        //printf("%s\n", payload);
        
        int i;
        for(i = 0; i < length; i++)
        {
            printf("%c", package[LENGTH_FID + i]);
        }
        printf("\n");
        
        if(DEBUG_FLAG)
        {
            printf("-----------------------------\n");
        }
		close(n);
	}

    return 0;
}


void printUsage()
{
	printf("Usage:\n");
	printf("server <sport>\n");
	printf("\tsport - Server's port number\n");
}